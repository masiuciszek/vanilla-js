const state = {
  language1: 'spanish',
  language2: 'french',
  language3: 'english'
};

const words = {
  english: {
    signIn: 'Sign In',
    email: 'Email Address',
    password: 'Password',
    remember: 'Remember Me'
  },
  french: {
    signIn: 'Se Connecter',
    email: 'Adresse Électronique',
    password: 'Mot de Passe',
    remember: 'Souviens-toi De Moi'
  },
  spanish: {
    signIn: 'Registrarse',
    email: 'Correo Electrónico',
    password: 'Contraseña',
    remember: 'Recuérdame'
  }
};

for (let val of Object.values(state)) {
  const { signIn, email, password, remember } = words[val];
  console.log(signIn);
}
